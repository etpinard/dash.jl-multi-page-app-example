# Dash.jl multi-page app example

Opinionated pattern for building maintainable, scalable and testable
[Dash.jl](https://github.com/plotly/Dash.jl) multi-page applications. Includes
example Julia [package](./DashMultiPageApp)!

## Features

- The app contains multiple pages routed as `/$(page_A)`, `/$(page_B)`, etc
- Support non-root base URL e.g. http://mydomain.xyz/mydashapp/
- Each page supports _sticky_ state via URL `?query` search strings
- Support browser-level navigation via the ← / → buttons or URL tweaking
- Common layout pieces, callbacks and handlers can be easily shared between pages
- Intuitive and scalable source directory structure
- Dash.jl callbacks logic and (maybe expansive) handlers are separated,
  for fast and thorough test runs
- Additional debug logging

## Demo

[![demo](./demo.gif)](./demo.gif)

## Discuss

- https://gitlab.com/etpinard/dash.jl-multi-page-app-example/-/issues
- https://community.plotly.com/t/opinionated-pattern-for-multi-page-dash-jl-apps/58567

## Core concepts

- [Define abstract page types](#define-abstract-page-types)
- [Page content](#page-content)
- [Shared page logic](#shared-page-logic)
- [Page router](#page-router)
- [Parse and stringify `?query` search string](#parse-and-stringify-query-search-string)
- [Mock the page types in unit tests](#mock-the-page-types-in-unit-tests)

### Define abstract page types

```jl
# "base" abstract type
abstract type AbstractPage end

# create one abstract type per page, for example
abstract type AbstractPageScatter end <: AbstractPage
abstract type AbstractPageSplom end <: AbstractPage

# subtype them from AbstractPage allows us
# to share generic methods, for example
function getroute(url_base_pathname::AbstractString, pg::AbstractPage)
    return "$(url_base_pathname)/$(pg.route)/"
end
```

### Page content

Each page has a corresponding concrete type. It can be useful to expose
page options to generalise your app for different context. For example, using

```jl
Base.@kwdef struct PageScatter <: AbstractPageScatter
    route::String = "scatter"
    tracecolor::Vector{String} = ["blue", "orange", "green"])
end
```

`PageScatter` can then be instantiated as

```jl
tracecolor = ["black", "grey", "brown"]
pg = PageScatter(; tracecolor)
```

and similarly,

```jl
Base.@kwdef struct PageSplom <: AbstractPageSplom
    route::String = "splom"
    markersize::Int = 8
end

pg = PageSplom(; markersize=20)
```

### Shared page logic

Now, for each page (say `AbstractPageA`) we define:

- `makelayout(pg::AbstractPageA)`
- `addcallbacks!(app::Dash.DashApp, pg::AbstractPageA)`

for complex apps, I'd recommend placing the `makelayout` definition in e.g.
`src/pages/page_A/layouts.jl` and the `addcallbacks!` definition in
`src/pages/page_A/callbacks.jl`.

In Dash.jl each component must have a unique id. Since Dash.jl keeps track of
ids throughout the app, we must be careful not to define component with the same
id on different pages. We workaround this constraint with the following helper
function:

```jl
prefix(pg::AbstractPage) = "__$(pg.route)__"
```

which can be used for example:

```jl
using Dash

# e.g. in src/pages/page_scatter/layouts.jl
function makelayout_graph(pg::AbstractPage; extraconfig...)
    pre = prefix(pg)
    toImageButtonOptions = (filename=pg.route,)
    return dcc_graph(; id="$pre-graph",
                     config=(; toImageButtonOptions, extraconfig...))
end

# then, define the page scatter graph with scroll zoom enabled
function makelayout_graph(pg::AbstractPageScatter)
    return makelayout_graph(pg; scrollZoom=true)
end

# and e.g. in src/pages/page_splom/layouts.jl
# define a custom modebar button set
function makelayout_graph(pg::AbstractPageSplom)
    modeBarButtons = [["toImage", "zoom2d", "pan2d", "lasso2d", "autoScale2d"]]
    return makelayout_graph(pg; modeBarButtons)
end
```

using a page-instance dependent `$pre` we ensure that an id never
appears more than once in the app!

### Page router

Starting from

```jl
app = dash(; suppress_callback_exceptions=true,
           prevent_initial_callbacks=true)
```

where we **need** `suppress_callback_exceptions=true` as the app's layout will
be dynamic and Dash.jl won't allow us to start the app without it.  In contrast,
we _opinionatedly_ set `prevent_initial_callbacks=true` to make the first render
logic more predictable (more on this in following subsection).

Then,

```jl
app.layout = html_div([dcc_location(; id="url"),
                       html_div(; id="content")])
```

this is our _root_ layout where we'll define a callback that takes in the
URL pathname, looks up which page is associated with the route and creates
the corresponding layout dynamically.

For example, we can create a route-to-page lookup as

```jl
# e.g.
pages = [PageScatter(), PageSplom()]

# then w.l.o.g
route2page = Dict{String,AbstractPage}()
for pg in pages
    route2page[pg.route] = pg
end
```

and the required callback is simply:

```jl
# works on *nix systems, needs tweaks for Windows ;)
normpathname(pathname) = normpath(pathname * "/")

callback!(app,
          Output("content", "children"),
          Input("url", "pathname")) do pathname
    npath = normpathname(pathname)
    return if haskey(route2page, npath)
        pg = route2page[npath]
        makelayout(pg)
    else
        404
    end
end
```

Note, we need to modify the above logic slightly to accommodate
`url_base_pathname` settings. Moreover, a _home_ page for the `/` route can be
useful. See the example package's [`app.jl`](./DashMultiPageApp/src/app.jl) for
a more complete implementation.

### Parse and stringify `?query` search string

To create _sticky_ state, where (most of) the page's state can be recreated
from a URL, we need to create two callbacks:

- one that takes all the component values we want to stringify and updates the
  URL with the stringified search query string, and
- one that takes the URL search string as input and parses it into component
  values.

To get browser-level navigation to work we need to make the
`dcc_location(; id="url")` component aware of changes to `?query` search part of
the URL. To do so, we turn the _root_ layout and callback into:

For example, starting with `app` from the previous section, in
`src/pages/page_scatter/layouts.jl`:

```jl
function makelayout(pg::AbstractPageScatter)
    pre = prefix(pg)
    options = [(label=x, value=x) for x in data_variables()]
    return html_div([dcc_dropdown(; id="$pre-dd-xvar", options),
                     dcc_dropdown(; id="$pre-dd-yvar", options)])
end
```

then in `src/pages/page_scatter/handlers.jl`:

```jl
using URIs: unescapeuri, queryparams

toparams(query) = queryparams(unescapeuri(lstrip(query, '?')))

# dummy getter, for presentation purposes
function data_variables()
    return ["PetalLength", "PetalWidth", "SepalLength", "SepalWidth"]
end

# note: general, could be moved in e.g. `src/utils.jl`
parseopt(params, key) = get(params, key, nothing)

function parseparams(::AbstractPageScatter, params)
    return (parseopt(params, "x"),
            parseopt(params, "y"))
end

# note: general, could be moved in e.g. `src/utils.jl`
makequeryopt(key, val::String) = "$key=$val"

# note: a general dispatch,
# could also be moved in e.g. `src/utils.jl`
function makequery(key2val::Pair...)
    return "?" * join([makequeryopt(key, val) for (key, val) in key2val], "&")
end

# note: page-specific dispatches that wrap the general dispatch
function makequery(::AbstractPageScatter, xvar::String, yvar::String)
    return makequery("x" => xvar, "y" => yvar)
end
makequery(::AbstractPageScatter, xvar, yvar) = ""
```

and finally in `src/pages/page_scatter/callbacks.jl`:

```jl
pg_scatter, pg_splom = pages

# note: we use `prevent_initial_call=false` to override
# the global `prevent_initial_callbacks=true` behaviour for
# this callback, so that it gets called
# as soon as the page scatter layout is rendered
callback!(app,
          Output("$pre-dd-xvar", "value"),
          Output("$pre-dd-yvar", "value"),
          Input("content", "children"),
          State("url", "search");
          prevent_initial_call=false) do _, query
    params = toparams(query)
    return parsehref(pg_scatter, query)
end

# note: this callback updates the page's URL
# which will then refresh the page by default,
# one can disable this by defining
# `dcc_location(id="url", refresh=false)`
# in the "root" layout
callback!(app,
          Output("url", "search"),
          Input("$pre-dd-xvar", "value"),
          Input("$pre-dd-yvar", "value")) do xvar, yvar
    return makequery(pg_scatter, xvar, yvar)
end

# Note: w/o setting `prevent_initial_callbacks=false` while
# instantiating the app, we could not guarantee that the
# "parse" callback would get called before the "serialize"
# callback. The situation can become even worse when many more
# callbacks are added. So I'd strongly recommend the
# `prevent_initial_callbacks=true` to make your Dash.jl apps much
# more predictable!
```

**But** this won't scale to multi-page apps as we would have multiple
callbacks with `Output("url", "search")` which is disallowed in Dash.jl!

We could group all the "serialize" logic into one callback like:

```jl
callback!(app,
          Output("url", "search"),
          Input("$(prefix(pg_scatter))-dd-xvar", "value"),
          Input("$(prefix-pg_scatter))-dd-yvar", "value"),
          # ...
          Input("$(prefix(pg_splom))-...", "value"),
          Input("$(prefix-pg_splom))-...", "value")) do args...
    prop_id = triggered() # more on `triggered` in following section
    return if startswith(prop_id, prefix(pg_scatter))
        makequery(pg_scatter, args...)
    elseif startswith(prop_id, prefix(pg_splom))
        makequery(pg_splom, args...)
    elseif #==#
        # ... and so on
    end
end
```

but keeping track of all the inputs for all pages can become annoying.

Using [pattern-matching callbacks](https://dash.plotly.com/pattern-matching-callbacks)
is an option if all the state is associated with `value` properties.
Unfortunately, this is not the case in general for example say we one want to
update the search query with some `figure.layout.xaxis.range`.

Thus, the example [package](./DashMultiPageApp) uses per-page `dcc_store`
to store the output of the pages' callback that generate the search query.
Those `dcc_store` data are then use as `Input` to update the `url.search`
property.

```jl
```jl
# in src/pages/page_scatter/layouts.jl
function makelayout(pg::AbstractPageScatter)
    pre = prefix(pg)
    options = [(label=x, value=x) for x in data_variables()]
    return html_div([dcc_store(; id="$pre-query"),
                     dcc_dropdown(; id="$pre-dd-xvar", options),
                     dcc_dropdown(; id="$pre-dd-yvar", options)])
end

# in src/pages/page_scatter/callbacks.jl
callback!(app,
          Output("$pre-query", "data"),
          Input("$pre-dd-xvar", "value"),
          Input("$pre-dd-yvar", "value")) do xvar, yvar
    return makequery(pg_scatter, xvar, yvar)
end
```

Then, our page routing logic becomes:

```jl
app.layout = html_div([dcc_location(; id="url", refresh=false),
                       # N.B. store "previous" path/query values
                       #      to determine if the route need to update the layout
                       dcc_store(; id="npath"),
                       dcc_store(; id="query"),
                       html_div(; id="content")])

callback!(app,
          Output("content", "children"),
          Input("url", "pathname"),
          Input("url", "href"),  # N.B. use `href`, not `search` here to avoid
                                 # a "Circular dependency" warning
          State("npath", "data")
          State("url", "search"),
          State("query", "data")) do pathname, _, npath2, query, query2
    npath = normpathname(pathname)
    return if haskey(route2page, npath)
        if (npath == npath2) && (query == query2)
            no_update()
        else
            pg = route2page[npath]
            makelayout(pg)
        end
    else
        404
    end
end

prefixes = prefix.(pages)
callback!(app,
          Output("npath", "data"),
          Output("query", "data"),
          Output("url", "search"),
          [Input("$pre-query", "data") for pre in prefixes]...,
          State("url", "pathname")) do args...
    queries..., pathname = args
    npath = normpathname(pathname)
    pg = route2page[npath]
    idx = findfirst(==(prefix(pg)), prefixes)
    query = !isnothing(idx) ? queries[idx] : ""
    return npath, query, query
end
```

where the `Output("url.search")` callback also update data of two `dcc_store`s
which we then use in the page router to determine if the `url.search` update
we made by the `Output("url.search")` callback or else by browser-level
navigation (e.g. pressing the back button).

The version in [`app.jl`](./DashMultiPageApp/src/app.jl) is slightly more
general where (1) browser-level navigation do not redraw the page layout
and (2) we parse the search query options inside the page router callback
making the sometimes-useful "first-render" values available to the page
callbacks, see
[`page_splom/callbacks.jl`](./DashMultiPageApp/src/pages/page_scatter/callbacks.jl)
for an example.

Note, if you plan on serializing more complex values, it might be best to use:

```jl
makequeryopt(key, val::T) = makequeryopt(key, escapeuri(JSON.json(val)))

function parseopt(params, key, dflt::T)
    return try
        JSON.parse(params[key])
    catch e
        dflt
    end
end

# where T could be any JSON-serializable type

# N.B. `URI(href)` un-escapes the query search string options,
# so no need to use `URIs.unescapeuri`.
```

### Mock the page types in unit tests

To test the callback logic, we define:

```jl
function triggered(::AbstractPage)
    t = Dash.callback_context().triggered
    return isempty(t) ? nothing : t[1].prop_id
end
```

taking the following callback

```jl
callbacks!(app,
           Output("pre-graph", "figure"),
           Input("$pre-graph", "selectedData"),
           Input("$pre-btn-clear", "n_clicks")) do seldata, _
    pts = if triggered(pg_splom) == "$pre-graph.selectedData"
        parseseldata(pg_splom, seldata)
    elseif triggered(pg_splom) == "$pre-btn-clear.n_clicks"
        []
    end
    return makefig(pg_splom, pts)
end
```

then in our tests:

```jl
using Test

using DashMultiPageApp
using DashMultiPageApp: prefix,
                        makelayout, addcallbacks!,
                        AbstractPage, AbstractPageSplom

using DashMultiPageApp.Dash

function mockapp(pg::AbstractPage)
    app = dash()
    app.layout = makelayout(pg)
    addcallbacks!(app, pg)
    return app
end

function getcbfn(app::Dash.DashApp, outid::String)
    return app.callbacks[Symbol(outid)].func
end
function getcbfn(app::Dash.DashApp, outids...)
    return getcbfn(app, "..$(join(outids, "..."))..")
end

# and in e.g. test/callbacks/page_splom.jl

Base.@kwdef struct MockPageSplom <: AbstractPageSplom
    route::String = "mock-splom"
end

@testset "PageSplom figure callback" begin
    pg = MockPageSplom()
    app = mockapp(pg)
    pre = prefix(pg)
    fn = getcbfn(app)

    # (optional) add a mocked `makefig` dispath to speed up the tests
    # or to better isolate the callback tests from the handler tests
    function DashMultiPageApp.makefig(::MockPageSplom, pts)
        #= faster implementation of `makefig` =#
    end

    @testset "on btn-clear clicks" begin
        DashMultiPageApp.triggered(::MockPageSplom) "$pre-btn-clear.n_clicks"
        @test fn("does not matter" 1) == #= fig with no selected points =#
    end
    @testset "on selectedData relays" begin
        DashMultiPageApp.triggered(::MockPageSplom) "$pre-graph.selectedData"
        seldata = #= some test seldata =#
        @test fn(seldata, "does not matter") == #= fig with selected points ! =#
    end
end
```

To test the handlers for different page options:

```jl
# e.g. in test/handlers/page_scatter.jl

using DashMultiPageApp
using DashMultiPageApp: AbstractPageScatter, makefig

pg_scatter0 = PageScatter()
Base.@kwdef struct ScatterMock <: AbstractPageScatter
    route::String = "mock-scatter"
    tracecolor = pg_scatter0.tracecolor
end

@testset "with dflt `tracecolor`" begin
    pg = ScatterMock()
    fig = makefig(pg, ["versicolor"], "SepalLength", "PetalLength")
    @test map(t -> t.marker.color, fig.data) == ["blue"]
end
@testset "with custom `tracecolor`" begin
    pg = ScatterMock(;tracecolor = ["yellow", "cyan", "magenta"])
    fig = makefig(pg, ["versicolor"], "SepalLength", "PetalLength")
    @test map(t -> t.marker.color, fig.data) == ["yellow"]
end
```

-------------------------

## License

[MIT](./LICENSE) - etpinard
