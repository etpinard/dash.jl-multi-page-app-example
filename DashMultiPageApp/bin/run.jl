using Pkg
Pkg.activate(joinpath(@__DIR__, ".."))

using DashMultiPageApp

runapp(8002,
       PageScatter(),
       PageSplom();
       url_base_pathname="/app/")
