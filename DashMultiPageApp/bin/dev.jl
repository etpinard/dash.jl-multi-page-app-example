using Pkg
Pkg.activate(joinpath(@__DIR__, ".."))

include("../src/DashMultiPageApp.jl")
using .DashMultiPageApp

# Turn on `@cbinfo` logging
using Logging
global_logger(ConsoleLogger(stdout, LogLevel_CBINFO))

const tracecolor = ["#e41a1c", "#377eb8", "#4daf4a"]
const markersize = 10

runapp(9002,
       PageScatter(; tracecolor),
       PageSplom(; markersize);
       debug=true,
       app_title="Debugging DashMultiPageApp.jl")
