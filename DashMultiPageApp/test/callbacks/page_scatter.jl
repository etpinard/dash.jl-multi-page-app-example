# the tests below are pretty useless, but written down from
# completeness and inspiration.
#
# PageScatter does not really need callback test as its callbacks
# only pass the inputs to the handlers without any additional logic

Base.@kwdef struct MockPageScatter <: AbstractPageScatter
    route::String = "mock-scatter"
end

@testset "PageScatter callbacks" begin
    pg = MockPageScatter()
    app = mockapp(pg)
    pre = prefix(pg)

    @testset "=>values" begin
        fn = getcbfn(app,
                     "$pre-cl-species.value",
                     "$pre-dd-xvar.value",
                     "$pre-dd-yvar.value")
        DashMultiPageApp.parseparams(::MockPageScatter, ::Any) = "dummy!"
        @test fn("whatever") == "dummy!"
    end

    @testset "=>graph.figure,url.search" begin
        fn = getcbfn(app,
                     "$pre-graph.figure",
                     "$pre-query.data")
        DashMultiPageApp.makefig(::MockPageScatter, ::Any, ::Any, ::Any) = "fig!"
        DashMultiPageApp.makequery(::MockPageScatter, ::Any, ::Any, ::Any) = "?query"
        @test fn("some species", "some xvar", "some yvar") == ("fig!", "?query")
    end
end
