pg_splom0 = PageSplom()
Base.@kwdef struct SplomMock <: AbstractPageSplom
    route::String = "mock-splom"
    markersize = pg_splom0.markersize
end

@testset "PageSplom parse query" begin
    pg = SplomMock()
    @test parsequery(pg, "") == []
    @test parsequery(pg, "?") == []
    @test parsequery(pg, "?notgonna=work") == []
    query = "pts=%5B%5B%5D%2C%5B%5D%2C%5B17%5D%5D"
    @test parsequery(pg, "?$query") == [[], [], [17]]
end

@testset "PageSplom parseseldata" begin
    pg = SplomMock()
    @test parseseldata(pg, nothing) == []
    seldata = (points=[(curveNumber=0, pointNumber=1),
                       (curveNumber=1, pointNumber=2)],)
    @test parseseldata(pg, seldata) == [[1], [2], []]
end

@testset "PageSplom makefig" begin
    fig = makefig(SplomMock(), [])
    @test map(t -> t.selectedpoints, fig.data) == [nothing, nothing, nothing]
    fig = makefig(SplomMock(), [[1], [2], []])
    @test map(t -> t.selectedpoints, fig.data) == [[1], [2], []]

    # note: testing different `markersize`

    @testset "with dflt `markersize`" begin
        pg = SplomMock()
        fig = makefig(pg, [])
        @test map(t -> t.marker.size, fig.data) == [8, 8, 8]
    end
    @testset "with custom `markersize`" begin
        pg = SplomMock(; markersize=20)
        fig = makefig(pg, [])
        @test map(t -> t.marker.size, fig.data) == [20, 20, 20]
    end
end

@testset "PageSplom maketabledata" begin
    pg = SplomMock()
    @test maketabledata(pg, []) == []
    @test maketabledata(pg, [[1], [2], []]) ==
          [Dict("SepalLength" => "4.9", "PetalWidth" => "0.2",
                "SepalWidth" => "3.0", "PetalLength" => "1.4", "Species" => "setosa"),
           Dict("SepalLength" => "6.9", "PetalWidth" => "1.5",
                "SepalWidth" => "3.1", "PetalLength" => "4.9", "Species" => "versicolor")]
end

@testset "PageSplom makequery" begin
    pg = SplomMock()
    @test makequery(pg, []) == ""
    @test makequery(pg, [[1], [2], []]) == "?pts=%5B%5B1%5D%2C%5B2%5D%2C%5B%5D%5D"
end
