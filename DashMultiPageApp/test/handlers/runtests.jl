using Test

using DashMultiPageApp
using DashMultiPageApp: AbstractPage, AbstractPageScatter, AbstractPageSplom,
                        toparams, parseparams, makefig, makequery,
                        parseseldata, maketabledata

parsequery(pg::AbstractPage, query) = parseparams(pg, toparams(query))

include("page_scatter.jl")
include("page_splom.jl")
