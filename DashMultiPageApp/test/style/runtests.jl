using Test

using DashMultiPageApp

using JuliaFormatter: format
using Aqua: test_all

@testset "Code style" begin
    @test format(joinpath(@__DIR__, "..", ".."); overwrite=false)
end

@testset "Aqua.jl checks" begin
    test_all(DashMultiPageApp; ambiguities=false)
end
