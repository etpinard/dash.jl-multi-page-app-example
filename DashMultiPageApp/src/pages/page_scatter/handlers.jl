const k_species = "species"
const k_x = "x"
const k_y = "y"

# general enough to be moved in e.g. src/utils.jl
function parseopt(params, key)
    return get(params, key, nothing)
end
function parseopt(params, key, dflt::AbstractVector)
    return try
        JSON.parse(params[key])
    catch e
        dflt
    end
end

function parseparams(::AbstractPageScatter, params)
    return (parseopt(params, k_species, []),
            parseopt(params, k_x),
            parseopt(params, k_y))
end

function makefig(pg::AbstractPageScatter, species::AbstractVector,
                 xvar::String, yvar::String)
    traces = Any[]
    for (i, name) in enumerate(species)
        d = DATA[DATA.Species .== name, :]
        push!(traces,
              (; type="scatter",
               mode="markers",
               x=d[!, Symbol(xvar)],
               y=d[!, Symbol(yvar)],
               marker=(color=pg.tracecolor[i],),
               name))
    end
    layout = (xaxis=(title=(text=xvar,),),
              yaxis=(title=(text=yvar,),))
    return (; data=traces, layout)
end
function makefig(::AbstractPageScatter, species, xvar, yvar)
    traces = [(x=[], y=[])]
    layout = NamedTuple()
    return (; data=traces, layout)
end

# general enough to be moved in e.g. src/utils.jl
makequeryopt(key, val::String) = "$key=$val"
makequeryopt(key, val::AbstractVector) = makequeryopt(key, escapeuri(JSON.json(val)))

# general enough to be moved in e.g. src/utils.jl
function makequery(key2val::Pair...)
    return "?" * join([makequeryopt(key, val) for (key, val) in key2val], "&")
end

# and similarly here, the PageScatter-specific dispatches
function makequery(::AbstractPageScatter, species::AbstractVector,
                   xvar::String, yvar::String)
    return isempty(species) ? "" : makequery(k_species => species, k_x => xvar, k_y => yvar)
end
makequery(::AbstractPageScatter, species, xvar, yvar) = ""
