function addcallbacks!(app::Dash.DashApp, pg::AbstractPageScatter)
    pre = prefix(pg)

    callback!(app,
              Output("$pre-cl-species", "value"),
              Output("$pre-dd-xvar", "value"),
              Output("$pre-dd-yvar", "value"),
              Input("params", "data");
              prevent_initial_call=false) do params
        @cbinfo "=>values..." params
        return parseparams(pg, params)
    end

    callback!(app,
              Output("$pre-graph", "figure"),
              Output("$pre-query", "data"),
              Input("$pre-cl-species", "value"),
              Input("$pre-dd-xvar", "value"),
              Input("$pre-dd-yvar", "value")) do species, xvar, yvar
        @cbinfo "=>graph.figure,query.data" species xvar yvar
        return (makefig(pg, species, xvar, yvar),
                makequery(pg, species, xvar, yvar))
    end

    return app
end
