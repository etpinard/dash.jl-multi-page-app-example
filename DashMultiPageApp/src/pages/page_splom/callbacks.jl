function addcallbacks!(app::Dash.DashApp, pg::AbstractPageSplom)
    pre = prefix(pg)

    # N.B. unfortunately we cannot simply place `Input("params", "data")`
    # in the `=>` callback below as Dash will complain that
    # the other `Input`s e.g. `"$pre-graph"` isn't present on the page
    callback!(app,
              Output("$pre-pts", "data"),
              Input("params", "data");
              prevent_initial_call=false) do params
        @cbinfo "=>pts" params
        return parseparams(pg, params)
    end

    callback!(app,
              Output("$pre-graph", "figure"),
              Output("$pre-table", "data"),
              Output("$pre-query", "data"),
              Input("$pre-pts", "data"),
              Input("$pre-graph", "selectedData"),
              Input("$pre-btn-clear", "n_clicks")) do pts0, seldata, _
        @cbinfo "=>" triggered(pg) pts0
        pts = if triggered(pg) == "$pre-pts.data"
            pts0
        elseif triggered(pg) == "$pre-btn-clear.n_clicks"
            []
        elseif triggered(pg) == "$pre-graph.selectedData"
            if !isnothing(seldata) && !haskey(seldata, :lassoPoints)
                @cbinfo "PreventUpdate workaround for weird plotly.js >=2.13 behaviour"
                return throw(PreventUpdate())
            else
                parseseldata(pg, seldata)
            end
        end
        @cbinfo "=>" pts
        return (makefig(pg, pts),
                maketabledata(pg, pts),
                makequery(pg, pts))
    end

    return app
end
