function makelayout(pg::AbstractPageHome)
    _name(pg::AbstractPage) = last(split(string(typeof(pg)), "."))
    items = html_ul([html_li(dcc_link(_name(pg); href=getroute(".", pg)))
                     for pg in pg.pages])
    return html_div([html_h1("Home page"), items])
end
