abstract type AbstractPage end
function makelayout(pg::AbstractPage) end
function addcallbacks!(app::Dash.DashApp, pg::AbstractPage) end

abstract type AbstractPageHome <: AbstractPage end
Base.@kwdef struct PageHome <: AbstractPageHome
    route::String = "/"
    pages = []
end
include("page_home/layouts.jl")

abstract type AbstractPageScatter <: AbstractPage end
Base.@kwdef struct PageScatter <: AbstractPageScatter
    route::String = "scatter"
    tracecolor::Vector{String} = ["blue", "orange", "orange"]
end
include("page_scatter/handlers.jl")
include("page_scatter/layouts.jl")
include("page_scatter/callbacks.jl")

abstract type AbstractPageSplom <: AbstractPage end
Base.@kwdef struct PageSplom <: AbstractPageSplom
    route::String = "splom"
    markersize::Int = 8
end
include("page_splom/handlers.jl")
include("page_splom/layouts.jl")
include("page_splom/callbacks.jl")

"""
Return page route.
"""
function getroute(url_base_pathname::AbstractString, pg::AbstractPage)
    return normpath("$(url_base_pathname)/$(pg.route)/")
end

"""
Return component id prefix.
"""
function prefix(pg::AbstractPage)
    return "__$(pg.route)__"
end

"""
Return the component id that triggered the callback.
To be used inside callback functions.

Defined as (::AbstractPage) to make it easy to mock.
"""
function triggered(::AbstractPage)
    t = Dash.callback_context().triggered
    return isempty(t) ? nothing : t[1].prop_id
end

export PageScatter, PageSplom
