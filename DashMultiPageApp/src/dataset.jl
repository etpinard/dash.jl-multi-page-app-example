# for nice example data
# (has nothing to do with the logic ;)
using RDatasets: dataset
const DATA = dataset("datasets", "iris")

data_names() = names(DATA)
data_species() = unique(DATA.Species)
data_variables() = setdiff(data_names(), ["Species"])
