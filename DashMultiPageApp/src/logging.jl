const LogLevel_CBINFO = LogLevel(-100)

"""
Custom log macro for callback info
with "importance" between @debug and @info

Inspired from
https://discourse.julialang.org/t/creating-a-custom-log-message-categorie/29295/14

Turn on `@cbinfo` logging with:
```
using Logging
global_logger(ConsoleLogger(stdout, -101))
```
"""
macro cbinfo(exprs...)
    quote
        @logmsg LogLevel_CBINFO $(map(x -> esc(x), exprs)...)
    end
end

export LogLevel_CBINFO
