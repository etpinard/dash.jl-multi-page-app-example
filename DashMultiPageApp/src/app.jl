const external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]

normpathname(pathname) = normpath(pathname * "/")

function toparams(query)
    return try
        queryparams(unescapeuri(lstrip(query, '?')))
    catch err
        Dict()
    end
end

"""
Instantiate dash app and setup root and page layouts and callbacks
"""
function makeapp(pages::AbstractPage...;
                 app_title="DashMultiPageApp example",
                 url_base_pathname="/",
                 kwargs...)
    # we must set this to `true` as we will be
    # defining callbacks for all the pages
    # before they become part of the layout
    suppress_callback_exceptions = true
    # make the app's callback execution order
    # more predictable!
    prevent_initial_callbacks = true
    # instantiate dash app and set the app title
    app = dash(; suppress_callback_exceptions,
               prevent_initial_callbacks,
               external_stylesheets,
               url_base_pathname,
               kwargs...)
    app.title = app_title

    # define the URL component
    # use refresh `false` to ensure that browser-level
    # navigation does not lead to page refresh
    refresh = false
    url = dcc_location(; id="url", refresh)

    # define "root" app layout
    layout_root = [url,
                   # stores for app-level `pathname` and `query`
                   dcc_store(; id="npath", data=""),
                   dcc_store(; id="query", data=""),
                   # page content goes here
                   html_div(; id="content"),
                   dcc_store(; id="params", data=Dict())]
    layout_for_every_page = map(collect(pages)) do pg
        pre = prefix(pg)
        return dcc_store(; id="$pre-query", data="")
    end
    app.layout = html_div(vcat(layout_root,
                               layout_for_every_page))

    # first check that the page routes are unique
    routes = collect(map(pg -> pg.route, pages))
    @assert unique(routes) == routes "Page routes are not unique!"
    # initialize route-to-page maps
    route2page = Dict{String,AbstractPage}()
    # instantiate the home page
    route2page[url_base_pathname] = PageHome(; pages)
    # instantiate the pages from the page specs
    for pg in pages
        route2page[getroute(url_base_pathname, pg)] = pg
    end

    # setup the update-search callback,
    # store the app-level `pathname` and `query`
    # to ensure the page router can distinguish
    # browser-level navigation from app-level changes
    prefixes = prefix.(pages)
    callback!(app,
              Output("npath", "data"),
              Output("url", "search"),
              Output("query", "data"),
              [Input("$pre-query", "data") for pre in prefixes]...,
              State("url", "pathname")) do args...
        queries..., pathname = args
        npath = normpathname(pathname)
        pg = route2page[npath]
        idx = findfirst(==(prefix(pg)), prefixes)
        query = !isnothing(idx) ? queries[idx] : ""
        @cbinfo "=>url.search" npath query
        return npath, query, query
    end

    # setup the root callback that fills in the page
    # given the `pathname` (aka the route)
    callback!(app,
              Output("content", "children"),
              Output("params", "data"),
              Input("url", "pathname"),
              # N.B. use `url.href` to avoid circular dependency warning
              Input("url", "href"),
              State("npath", "data"),
              State("url", "search"),
              State("query", "data")) do pathname, _, npath2, query, query2
        npath = normpathname(pathname)
        @cbinfo "=>content.children" npath npath2 query query2
        return if haskey(route2page, npath)
            pg = route2page[npath]
            if npath == npath2
                if query == query2
                    @cbinfo "! NO UPDATE"
                    no_update(), no_update()
                else
                    @cbinfo "PARAMS UPDATE"
                    params = toparams(query)
                    no_update(), params
                end
            else
                @cbinfo "PAGE UPDATE"
                makelayout(pg), toparams(query)
            end
        else
            404
        end
    end

    # setup the page callbacks
    for pg in values(route2page)
        addcallbacks!(app, pg)
    end

    return app
end

"""
Instantiate and run the DashMultiPageApp dash app!

Accept all `Dash.dash` kwargs except for
`suppress_callback_exceptions` (set to true)
`prevent_initial_callbacks` (set to true) and
`external_stylesheets`

Implements addition kwarg `app_title::String` to
set the app's document title.

Examples:

runapp(8010, PageScatter())

runapp(8011, PageScatter(); debug=false, app_title="DEBUGGING")

runapp(8012, PageScatter(), PageSplom(); url_base_pathname="/app/", update_title="please wait ... ...")

runapp(8013, PageSplom(;route="one"), PageSplom(;route="two", markersize=2))

"""
function runapp(port::Int,
                pages::AbstractPage...;
                debug=false,
                kwargs...)
    app = makeapp(pages...; kwargs...)
    return run_server(app, "0.0.0.0", port; debug)
end

export runapp
